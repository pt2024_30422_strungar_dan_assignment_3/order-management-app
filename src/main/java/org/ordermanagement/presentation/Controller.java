package org.ordermanagement.presentation;

import org.ordermanagement.business_logic.BillBLL;
import org.ordermanagement.business_logic.ClientBLL;
import org.ordermanagement.business_logic.OrderBLL;
import org.ordermanagement.business_logic.OrderItemBLL;
import org.ordermanagement.business_logic.ProductBLL;

/**
 * The Controller class serves as a central hub for accessing various business logic components.
 * It provides access to different BLL (Business Logic Layer) instances such as ClientBLL, OrderBLL, etc.
 */
public class Controller {

    private ClientBLL clientBLL;
    private OrderBLL orderBLL;
    private OrderItemBLL orderItemBLL;
    private ProductBLL productBLL;
    private BillBLL billBLL;

    /**
     * Constructs a new Controller object and initializes all BLL instances.
     */
    public Controller() {
        this.clientBLL = new ClientBLL();
        this.orderBLL = new OrderBLL();
        this.orderItemBLL = new OrderItemBLL();
        this.productBLL = new ProductBLL();
        this.billBLL = new BillBLL();
    }

    /**
     * Gets the ClientBLL instance.
     *
     * @return The ClientBLL instance.
     */
    public ClientBLL getClientBLL() {
        return clientBLL;
    }

    /**
     * Gets the OrderBLL instance.
     *
     * @return The OrderBLL instance.
     */
    public OrderBLL getOrderBLL() {
        return orderBLL;
    }

    /**
     * Gets the OrderItemBLL instance.
     *
     * @return The OrderItemBLL instance.
     */
    public OrderItemBLL getOrderItemBLL() {
        return orderItemBLL;
    }

    /**
     * Gets the ProductBLL instance.
     *
     * @return The ProductBLL instance.
     */
    public ProductBLL getProductBLL() {
        return productBLL;
    }

    /**
     * Gets the BillBLL instance.
     *
     * @return The BillBLL instance.
     */
    public BillBLL getBillBLL() {
        return billBLL;
    }
}
