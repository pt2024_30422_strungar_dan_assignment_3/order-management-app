package org.ordermanagement.presentation;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import org.ordermanagement.model.Bill;
import org.ordermanagement.model.Order;
import org.ordermanagement.model.OrderItem;
import org.ordermanagement.model.Product;

import java.awt.*;
import java.awt.event.*;
import java.awt.font.NumericShaper;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The OrderPanel class represents the graphical user interface for managing orders.
 * It allows users to add new orders, delete existing orders, list all orders, and view order details.
 */
public class OrderPanel extends JPanel
{

	private MainGUI mainGUI; // Reference to the MainGUI class
	private JPanel utilPanel = new JPanel();
	private JTable table;
	private DefaultTableModel tableModel;
	private JPanel addOrderPanel, deletePanel, listAllPanel, ordersPanel;
	private JTextField idClientFieldInsert;
	private JTextField idClientFieldDelete;
	private HashMap<Product, Integer> tempOrder = new HashMap<>();
	
	private StringBuilder prodList = new StringBuilder();
	private double total_Price = 0.0;
	
	/**
     * Constructs a new OrderPanel object.
     *
     * @param mainGUI The reference to the MainGUI.
     */
	public OrderPanel(MainGUI mainGUI)
	{
		this.mainGUI = mainGUI; // Store reference to the MainGUI

		setLayout(new BorderLayout());

		JPanel titlePanel = new JPanel();
		JLabel titleLabel = new JLabel("ORDER MANAGER WINDOW");
		titlePanel.add(titleLabel);

		JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS)); // Set layout to BoxLayout with Y_AXIS
																			// alignment

		// Add vertical glue to center the button panel vertically
		leftPanel.add(Box.createVerticalGlue());

		// Create a separate BoxLayout for buttonPanel
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));

		// Create and add 5 buttons to the button panel with rigid areas between them
		JButton createButton = new JButton("ADD NEW");
		createButton.setMaximumSize(new Dimension(150, 30));
		createButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				((CardLayout) utilPanel.getLayout()).show(utilPanel, "ADD_ORDER_PANEL");
			}
		});
		buttonPanel.add(createButton);
		buttonPanel.add(Box.createRigidArea(new Dimension(0, 20))); // Add 5 pixels of vertical space

		JButton deleteButton = new JButton("DELETE");
		deleteButton.setMaximumSize(new Dimension(150, 30));
		deleteButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				((CardLayout) utilPanel.getLayout()).show(utilPanel, "DELETE_ORDER_PANEL");
			}
		});
		buttonPanel.add(deleteButton);
		buttonPanel.add(Box.createRigidArea(new Dimension(0, 20))); // Add 5 pixels of vertical space

		JButton listAllButton = new JButton("LIST ALL");
		listAllButton.setMaximumSize(new Dimension(150, 30));
		listAllButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				updateOrdersPanel();
				((CardLayout) utilPanel.getLayout()).show(utilPanel, "LIST_ALL_PANEL");

			}
		});
		buttonPanel.add(listAllButton);
		buttonPanel.add(Box.createRigidArea(new Dimension(0, 20))); // Add 5 pixels of vertical space

		JButton backButton = new JButton("BACK");
		backButton.setMaximumSize(new Dimension(150, 30));
		backButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				mainGUI.showStartPanel();
			}
		});
		buttonPanel.add(backButton);

		// Add the button panel to the left panel
		leftPanel.add(buttonPanel);

		// Add more vertical glue to center the button panel vertically
		leftPanel.add(Box.createVerticalGlue());

		// Align the button panel in the center horizontally
		buttonPanel.setAlignmentX(Component.CENTER_ALIGNMENT);

		leftPanel.setPreferredSize(new Dimension(300, 100));

		utilPanel.setLayout(new CardLayout());

		JLayeredPane contentPane = new JLayeredPane();

		createListAllPanel();
		utilPanel.add(listAllPanel, "LIST_ALL_PANEL");
		createAddOrderPanel();
		utilPanel.add(addOrderPanel, "ADD_ORDER_PANEL");
		createDeleteProductPanel();
		utilPanel.add(deletePanel, "DELETE_ORDER_PANEL");

		utilPanel.add(contentPane);

		add(titlePanel, BorderLayout.NORTH);
		add(leftPanel, BorderLayout.WEST);
		add(utilPanel, BorderLayout.CENTER);

		((CardLayout) utilPanel.getLayout()).show(utilPanel, "LIST_ALL_PANEL");

	}
	
	/**
	 * The OrderPanel class represents the graphical user interface for managing orders.
	 * It allows users to add new orders, delete existing orders, list all orders, and view order details.
	 */
	private void createListAllPanel() {
        listAllPanel = new JPanel();
        listAllPanel.setLayout(new BorderLayout());

        JLabel titleLabel = new JLabel("Order List");
        listAllPanel.add(titleLabel, BorderLayout.NORTH);

        ordersPanel = new JPanel();
        ordersPanel.setLayout(new GridLayout(0, 1, 10, 10)); // 1 column, multiple rows, 10px gaps

        JScrollPane scrollPane = new JScrollPane(ordersPanel);
        listAllPanel.add(scrollPane, BorderLayout.CENTER);

        updateOrdersPanel();
    }

	/**
     * Updates the panel displaying all orders.
     */
    private void updateOrdersPanel() {
        ordersPanel.removeAll(); // Clear existing components
        List<Order> orders = mainGUI.getController().getOrderBLL().findAll();
        if(orders != null) {
        	for (Order order : orders) {
                JPanel orderPanel = new JPanel();
                orderPanel.setLayout(new GridLayout(1, 4, 10, 10)); // 4 columns: ID, Client ID, Date, Details Button

                JLabel idLabel = new JLabel("ID: " + order.getOrder_id());
                JLabel clientIdLabel = new JLabel("Client ID: " + order.getClient_id());
                JLabel dateLabel = new JLabel("Date: " + order.getOrder_date().toString());
                JButton detailsButton = new JButton("Details");

                detailsButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        showOrderDetails(order);
                    }
                });

                orderPanel.add(idLabel);
                orderPanel.add(clientIdLabel);
                orderPanel.add(dateLabel);
                orderPanel.add(detailsButton);

                ordersPanel.add(orderPanel);
            }
        }
        ordersPanel.revalidate();
        ordersPanel.repaint();
    }

    /**
     * Displays the details of a specific order.
     *
     * @param order The order to display details for.
     */
    private void showOrderDetails(Order order) {
        // Implement the logic to show order details
        JFrame productFrame = new JFrame("Order " + order.getOrder_id() + " contains the following products:");
        productFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        productFrame.setLayout(new BorderLayout());
		productFrame.setSize(1000, 500);
		productFrame.setVisible(true);
		
		JPanel productPanel = new JPanel();
		productPanel.setLayout(new GridLayout(0, 3, 10, 10));
		
		List<OrderItem> items = mainGUI.getController().getOrderItemBLL().findById(order.getOrder_id());
		
		for (OrderItem orderItem : items)
		{
			Product product = mainGUI.getController().getProductBLL().findById(orderItem.getProduct_id());
			JLabel productIdLabel = new JLabel(product.getProduct_id().toString());
			JLabel productNameLabel = new JLabel(product.getProduct_name());
			Integer aux = orderItem.getQuantity();
			JLabel quant = new JLabel(aux.toString());
			
			productPanel.add(productIdLabel);
			productPanel.add(productNameLabel);
			productPanel.add(quant);
		}
		productFrame.add(new JScrollPane(productPanel), BorderLayout.CENTER);
    }

    /**
     * Creates the panel for adding a new order.
     */
	private void createAddOrderPanel()
	{
		addOrderPanel = new JPanel();

		addOrderPanel.setLayout(new BorderLayout());

		JLabel titleLabel = new JLabel("INSERT WINDOW");
		addOrderPanel.add(titleLabel, BorderLayout.NORTH);

		JPanel contentPanel = new JPanel();
		contentPanel.setLayout(new GridLayout(1, 2));
		contentPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		contentPanel.add(new JLabel("Client ID:"));
		idClientFieldInsert = new JTextField();
		contentPanel.add(idClientFieldInsert);

		addOrderPanel.add(contentPanel, BorderLayout.CENTER);

		JButton insertButton = new JButton("INSERT");
		insertButton.setMaximumSize(new Dimension(150, 30));
		insertButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				System.out.println(idClientFieldInsert.getText().toString());

				Integer clientId = Integer.parseInt(idClientFieldInsert.getText().toString());

				if (mainGUI.getController().getClientBLL().findById(clientId) == null)
				{
					JOptionPane.showMessageDialog(null,
							"Client with id " + idClientFieldInsert.getText().toString() + " does not exist!!",
							"WARNING", JOptionPane.INFORMATION_MESSAGE);
				} else
				{
					Order orderToInsert = new Order(clientId, LocalDateTime.now());
					Order test = mainGUI.getController().getOrderBLL().insert(orderToInsert);
					String clientName = mainGUI.getController().getClientBLL().findById(clientId).getClient_name();
					createAddProductFrame(test.getOrder_id(), clientName);
				}
			}
		});

		addOrderPanel.add(insertButton, BorderLayout.SOUTH);

		// Hide the panel initially
		addOrderPanel.setVisible(true);

		// add(addClientPanel, BorderLayout.CENTER); // Add the add client panel to the
		// bottom of the main panel
	}

	/**
     * Creates a frame for adding products to a new order.
     *
     * @param orderId    The ID of the order being created.
     * @param clientName The name of the client associated with the order.
     */
	private void createAddProductFrame(Integer orderId, String clientName)
	{
		JFrame productFrame = new JFrame("Order for client with id " + orderId);
		productFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		productFrame.setSize(1000, 500);
		productFrame.setVisible(true);

		JPanel productPanel = new JPanel();
		productPanel.setLayout(new GridLayout(0, 4, 10, 10));

		List<Product> products = mainGUI.getController().getProductBLL().findAll();
		
		for (Product product : products)
		{
			JLabel productLabel = new JLabel(product.getProduct_name());
			JSpinner numberSpinner = new JSpinner(new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1));
			JFormattedTextField quantityField = ((JSpinner.DefaultEditor) numberSpinner.getEditor()).getTextField();
			JButton addButton = new JButton("Add");
			JButton clearButton = new JButton("Clear");

			addButton.addActionListener(new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					int quantity = Integer.parseInt(quantityField.getText());
					// Add product to order logic here
					if (tempOrder.containsKey(product))
					{
						JOptionPane.showMessageDialog(null,
								"Product " + product.getProduct_name() + " has already been added!!", "WARNING",
								JOptionPane.INFORMATION_MESSAGE);
					}
					else
					{
						Integer maxQuantity = mainGUI.getController().getProductBLL().findById(product.getProduct_id()).getStock_quantity();
						if(maxQuantity < quantity) {
							JOptionPane.showMessageDialog(null,
									"Not enough stock for product " + product.getProduct_name() + " max ammount is: " + maxQuantity, "WARNING",
									JOptionPane.INFORMATION_MESSAGE);
						}
						else {
							tempOrder.put(product, quantity);
							JOptionPane.showMessageDialog(null,
									"Product " + product.getProduct_name() + " has been added to temporary cart!!", "WARNING",
									JOptionPane.INFORMATION_MESSAGE);
							numberSpinner.setEnabled(false);
						}
					}
					// mainGUI.getController().getOrderBLL().addProductToOrder(product, quantity);
					// JOptionPane.showMessageDialog(productFrame, "Added " + quantity + " of " +
					// product.getName());
				}
			});

			clearButton.addActionListener(new ActionListener()
			{

				@Override
				public void actionPerformed(ActionEvent e)
				{
					if(tempOrder.containsKey(product)) {
						tempOrder.remove(product);
						numberSpinner.setEnabled(true);
						quantityField.setText("1");
						JOptionPane.showMessageDialog(null,
								"Product " + product.getProduct_name() + " has been removed from temporary cart!!", "WARNING",
								JOptionPane.INFORMATION_MESSAGE);
					}
					else {
						JOptionPane.showMessageDialog(null,
								"Product " + product.getProduct_name() + " is not present in the temporary cart therefore it cannot be romoved!!", "WARNING",
								JOptionPane.INFORMATION_MESSAGE);
					}
				}
			});

			productPanel.add(productLabel);
			productPanel.add(numberSpinner);
			productPanel.add(addButton);
			productPanel.add(clearButton);
		}
		
		productFrame.add(new JScrollPane(productPanel), BorderLayout.CENTER);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(1, 2, 10, 10));
		
		JButton finishButton = new JButton("Finish");
		JButton viewButton = new JButton("View");
		
		buttonPanel.add(finishButton);
		buttonPanel.add(viewButton);
		
		
		
		viewButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				showCart();
			}
		});
		
		
		
		finishButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				for (Product product : tempOrder.keySet())
				{
					System.out.println(orderId);
					mainGUI.getController().getOrderItemBLL().insert(new OrderItem(orderId, product.getProduct_id(), tempOrder.get(product)));
					total_Price += product.getPrice();
					prodList.append(product.getProduct_name());
					prodList.append(" ");
				}
				prodList.deleteCharAt(prodList.length() - 1);
				
				mainGUI.getController().getBillBLL().insert(new Bill(orderId, clientName, prodList.toString(), total_Price));
				
				JOptionPane.showMessageDialog(null,
						"Order created successfully!!", "WARNING",
						JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		productFrame.add(buttonPanel, BorderLayout.SOUTH);
		
		productFrame.setVisible(true);
	}
	
	/**
     * Shows the contents of the temporary cart.
     */
	private void showCart() {
        JFrame cartFrame = new JFrame("Cart");
        cartFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        cartFrame.setSize(400, 600);

        JPanel cartPanel = new JPanel();
        cartPanel.setLayout(new GridLayout(0, 2, 10, 10)); // 2 columns for product name and quantity

        for (Map.Entry<Product, Integer> entry : tempOrder.entrySet()) {
            JLabel productLabel = new JLabel(entry.getKey().getProduct_name());
            JLabel quantityLabel = new JLabel(entry.getValue().toString());

            cartPanel.add(productLabel);
            cartPanel.add(quantityLabel);
        }

        cartFrame.add(new JScrollPane(cartPanel), BorderLayout.CENTER);
        cartFrame.setVisible(true);
    }

	 /**
     * Creates the panel for deleting an existing order.
     */
	private void createDeleteProductPanel()
	{
		deletePanel = new JPanel();
		
		deletePanel.setLayout(new BorderLayout()); 
		
		JLabel titleLabel = new JLabel("DELETE WINDOW");
		deletePanel.add(titleLabel, BorderLayout.NORTH);

		JPanel contentPanel = new JPanel();
		contentPanel.setLayout(new GridLayout(1, 2));
		contentPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		contentPanel.add(new JLabel("ID:"));
		idClientFieldDelete = new JTextField();
		contentPanel.add(idClientFieldDelete);
		
		deletePanel.add(contentPanel, BorderLayout.CENTER);

		JButton deleteButton = new JButton("DELETE");
		deleteButton.setMaximumSize(new Dimension(150, 30));
		deleteButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				Order order = mainGUI.getController().getOrderBLL().findById(Integer.parseInt(idClientFieldDelete.getText().toString()));
				//System.out.println(product);
				
				if (order == null)	
				{
					JOptionPane.showMessageDialog(null, "Order with id " + idClientFieldDelete.getText().toString() + " does not exist!!", "WARNING", JOptionPane.INFORMATION_MESSAGE);
				}
				else {
					List<OrderItem> items = mainGUI.getController().getOrderItemBLL().findById(order.getOrder_id());
					for (OrderItem orderItem : items)
					{
						mainGUI.getController().getOrderItemBLL().delete(orderItem);
					}
					mainGUI.getController().getOrderBLL().delete(order);
					JOptionPane.showMessageDialog(null, "DELETE SUCCESSFUL", "WARNING", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});

		deletePanel.add(deleteButton, BorderLayout.SOUTH);

		// Hide the panel initially
		deletePanel.setVisible(true);

		// add(deletePanel, BorderLayout.CENTER); // Add the add client panel to the
		// bottom of the main panel
		
	}
}
