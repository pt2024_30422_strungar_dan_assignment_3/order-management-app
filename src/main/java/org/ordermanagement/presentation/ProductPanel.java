package org.ordermanagement.presentation;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import org.ordermanagement.business_logic.ClientBLL;
import org.ordermanagement.model.Client;
import org.ordermanagement.model.Product;

import java.awt.*;
import java.awt.event.*;
import java.util.List;

/**
 * The ProductPanel class represents the graphical user interface for managing products.
 * It allows users to add new products, update existing products, delete products, and list all products.
 */
public class ProductPanel extends JPanel
{
	
	private MainGUI mainGUI; // Reference to the MainGUI class
	private JPanel utilPanel = new JPanel();
	private JTable table;
	private DefaultTableModel tableModel;
	private JPanel addProductPanel, updatePanel, deletePanel, listAllPanel;
	private JTextField idFieldInsert, nameFieldInsert, priceFieldInsert, stockFieldInsert;
	private JTextField idFieldUpdate, nameFieldUpdate, priceFieldUpdate, stockFieldUpdate;
	private JTextField idFieldDelete, nameFieldDelete, priceFieldDelete, stockFieldDelete;

	/**
     * Constructs a new ProductPanel object.
     *
     * @param mainGUI The reference to the MainGUI.
     */
	public ProductPanel(MainGUI mainGUI)
	{
		this.mainGUI = mainGUI; // Store reference to the MainGUI

		setLayout(new BorderLayout());

		JPanel titlePanel = new JPanel();
		JLabel titleLabel = new JLabel("PRODUCT MANAGER WINDOW");
		titlePanel.add(titleLabel);

		JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS)); // Set layout to BoxLayout with Y_AXIS
																			// alignment

		// Add vertical glue to center the button panel vertically
		leftPanel.add(Box.createVerticalGlue());

		// Create a separate BoxLayout for buttonPanel
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));

		// Create and add 5 buttons to the button panel with rigid areas between them
		JButton createButton = new JButton("ADD NEW");
		createButton.setMaximumSize(new Dimension(150, 30));
		createButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				((CardLayout) utilPanel.getLayout()).show(utilPanel, "ADD_PRODUCT_PANEL");
			}
		});
		buttonPanel.add(createButton);
		buttonPanel.add(Box.createRigidArea(new Dimension(0, 20))); // Add 5 pixels of vertical space

		JButton updateButton = new JButton("UPDATE");
		updateButton.setMaximumSize(new Dimension(150, 30));
		updateButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				((CardLayout) utilPanel.getLayout()).show(utilPanel, "UPDATE_PRODUCT_PANEL");
			}
		});
		buttonPanel.add(updateButton);
		buttonPanel.add(Box.createRigidArea(new Dimension(0, 20))); // Add 5 pixels of vertical space

		JButton deleteButton = new JButton("DELETE");
		deleteButton.setMaximumSize(new Dimension(150, 30));
		deleteButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				((CardLayout) utilPanel.getLayout()).show(utilPanel, "DELETE_PRODUCT_PANEL");
			}
		});
		buttonPanel.add(deleteButton);
		buttonPanel.add(Box.createRigidArea(new Dimension(0, 20))); // Add 5 pixels of vertical space

		JButton listAllButton = new JButton("LIST ALL");
		listAllButton.setMaximumSize(new Dimension(150, 30));
		listAllButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				updateTable();
				((CardLayout) utilPanel.getLayout()).show(utilPanel, "LIST_ALL_PANEL");

			}
		});
		buttonPanel.add(listAllButton);
		buttonPanel.add(Box.createRigidArea(new Dimension(0, 20))); // Add 5 pixels of vertical space

		JButton backButton = new JButton("BACK");
		backButton.setMaximumSize(new Dimension(150, 30));
		backButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				mainGUI.showStartPanel();
			}
		});
		buttonPanel.add(backButton);

		// Add the button panel to the left panel
		leftPanel.add(buttonPanel);

		// Add more vertical glue to center the button panel vertically
		leftPanel.add(Box.createVerticalGlue());

		// Align the button panel in the center horizontally
		buttonPanel.setAlignmentX(Component.CENTER_ALIGNMENT);

		leftPanel.setPreferredSize(new Dimension(300, 100));

		utilPanel.setLayout(new CardLayout());

		JLayeredPane contentPane = new JLayeredPane();

		createListAllPanel();
		utilPanel.add(listAllPanel, "LIST_ALL_PANEL");
		createAddProductPanel();
		utilPanel.add(addProductPanel, "ADD_PRODUCT_PANEL");
		createUpdateProductPanel();
		utilPanel.add(updatePanel, "UPDATE_PRODUCT_PANEL");
		createDeleteProductPanel();
		utilPanel.add(deletePanel, "DELETE_PRODUCT_PANEL");

		utilPanel.add(contentPane);

		add(titlePanel, BorderLayout.NORTH);
		add(leftPanel, BorderLayout.WEST);
		add(utilPanel, BorderLayout.CENTER);

		((CardLayout) utilPanel.getLayout()).show(utilPanel, "LIST_ALL_PANEL");

	}

	/**
     * Creates the panel for listing all products.
     */
	private void createListAllPanel()
	{
		listAllPanel = new JPanel();
		listAllPanel.setLayout(new BorderLayout());

		tableModel = new DefaultTableModel();

		String[] columnNames =
		{ "ID", "Name", "price", "stock" };

		for (String columnName : columnNames)
		{
			tableModel.addColumn(columnName);
		}

		table = new JTable(tableModel);

		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane scrollPane = new JScrollPane(table);

		listAllPanel.add(scrollPane, BorderLayout.CENTER);

		JLabel titleLabel = new JLabel("Product List");
		listAllPanel.add(titleLabel, BorderLayout.NORTH);
		updateTable();
	}

	/**
     * Updates the table displaying all products.
     */
	private void updateTable()
	{
		List<Product> products = mainGUI.getController().getProductBLL().findAll();
		if (tableModel.getRowCount() > 0)
		{
			for (int i = tableModel.getRowCount() - 1; i >= 0; i--)
			{
				tableModel.removeRow(i);
			}
		}
		for (Product product : products)
		{
			System.out.println(product);
			Object[] row =
			{ product.getProduct_id(), product.getProduct_name(), product.getPrice(),
					product.getStock_quantity() };
			tableModel.addRow(row);
		}

	}

	/**
     * Creates the panel for adding a new product.
     */
	private void createAddProductPanel()
	{
		addProductPanel = new JPanel();

		addProductPanel.setLayout(new BorderLayout());

		JLabel titleLabel = new JLabel("INSERT WINDOW");
		addProductPanel.add(titleLabel, BorderLayout.NORTH);

		JPanel contentPanel = new JPanel();
		contentPanel.setLayout(new GridLayout(3, 2));
		contentPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		contentPanel.add(new JLabel("Name:"));
		nameFieldInsert = new JTextField();
		contentPanel.add(nameFieldInsert);

		contentPanel.add(new JLabel("Stock:"));
		stockFieldInsert = new JTextField();
		contentPanel.add(stockFieldInsert);

		contentPanel.add(new JLabel("Price:"));
		priceFieldInsert = new JTextField();
		contentPanel.add(priceFieldInsert);

		addProductPanel.add(contentPanel, BorderLayout.CENTER);

		JButton insertButton = new JButton("INSERT");
		insertButton.setMaximumSize(new Dimension(150, 30));
		insertButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				System.out.println(nameFieldInsert.getText().toString());
				System.out.println(stockFieldInsert.getText().toString());
				System.out.println(priceFieldInsert.getText().toString());
				
				if(Integer.parseInt(stockFieldInsert.getText().toString()) < 0) {
					JOptionPane.showMessageDialog(null, "CANNOT ADD NEGATIVE STOCK!!", "WARNING", JOptionPane.INFORMATION_MESSAGE);
				}
				else if(Double.parseDouble(priceFieldInsert.getText().toString()) <= 0.0) {
					JOptionPane.showMessageDialog(null, "CANNOT ADD NEGATIVE PRICE!!", "WARNING", JOptionPane.INFORMATION_MESSAGE);
				}
				else {
					mainGUI.getController().getProductBLL()
					.insert(new Product(nameFieldInsert.getText().toString(),
							Integer.parseInt(stockFieldInsert.getText().toString()), Double.parseDouble(priceFieldInsert.getText().toString())));
				}
			}
		});

		addProductPanel.add(insertButton, BorderLayout.SOUTH);

		// Hide the panel initially
		addProductPanel.setVisible(true);

		// add(addClientPanel, BorderLayout.CENTER); // Add the add client panel to the
		// bottom of the main panel
	}

	/**
     * Creates the panel for updating an existing product.
     */
	private void createUpdateProductPanel()
	{
		updatePanel = new JPanel();

		updatePanel.setLayout(new BorderLayout());

		JLabel titleLabel = new JLabel("UPDATE WINDOW");
		updatePanel.add(titleLabel, BorderLayout.NORTH);

		JPanel contentPanel = new JPanel();
		contentPanel.setLayout(new GridLayout(4, 2));
		contentPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		contentPanel.add(new JLabel("ID:"));
		idFieldUpdate = new JTextField();
		contentPanel.add(idFieldUpdate);

		contentPanel.add(new JLabel("Name:"));
		nameFieldUpdate = new JTextField();
		contentPanel.add(nameFieldUpdate);

		contentPanel.add(new JLabel("Stock:"));
		stockFieldUpdate = new JTextField();
		contentPanel.add(stockFieldUpdate);

		contentPanel.add(new JLabel("Price:"));
		priceFieldUpdate = new JTextField();
		contentPanel.add(priceFieldUpdate);

		updatePanel.add(contentPanel, BorderLayout.CENTER);

		JButton updateButton = new JButton("UPDATE");
		updateButton.setMaximumSize(new Dimension(150, 30));
		updateButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				System.out.println(idFieldUpdate.getText().toString());
				System.out.println(nameFieldUpdate.getText().toString());
				System.out.println(stockFieldUpdate.getText().toString());
				System.out.println(priceFieldUpdate.getText().toString());
				Product product = mainGUI.getController().getProductBLL().findById(Integer.parseInt(idFieldUpdate.getText().toString()));
				System.out.println(product);
				
				if (product == null)	
				{
					JOptionPane.showMessageDialog(null, "Client with id " + idFieldUpdate.getText().toString() + " does not exist!!", "WARNING", JOptionPane.INFORMATION_MESSAGE);
				}
				else if(Double.parseDouble(stockFieldUpdate.getText().toString()) <= 0) {
					JOptionPane.showMessageDialog(null, "CANNOT ADD NEGATIVE STOCK!!", "WARNING", JOptionPane.INFORMATION_MESSAGE);
				}
				else if(Double.parseDouble(priceFieldUpdate.getText().toString()) <= 0.0) {
					JOptionPane.showMessageDialog(null, "CANNOT ADD NEGATIVE PRICE!!", "WARNING", JOptionPane.INFORMATION_MESSAGE);
				}
				else {
					product.setProduct_name(nameFieldUpdate.getText().toString());
					product.setStock_quantity(Integer.parseInt(stockFieldUpdate.getText().toString()));
					product.setPrice(Double.parseDouble(priceFieldUpdate.getText().toString()));
					mainGUI.getController().getProductBLL().update(product);
					JOptionPane.showMessageDialog(null, "UPDATE SUCCESSFUL", "WARNING", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});

		updatePanel.add(updateButton, BorderLayout.SOUTH);

		// Hide the panel initially
		updatePanel.setVisible(true);

		// add(updatePanel, BorderLayout.CENTER); // Add the add client panel to the
		// bottom of the main panel
	}

	/**
     * Creates the panel for deleting an existing product.
     */
	private void createDeleteProductPanel()
	{
		deletePanel = new JPanel();
		
		deletePanel.setLayout(new BorderLayout()); 
		
		JLabel titleLabel = new JLabel("DELETE WINDOW");
		deletePanel.add(titleLabel, BorderLayout.NORTH);

		JPanel contentPanel = new JPanel();
		contentPanel.setLayout(new GridLayout(1, 2));
		contentPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		contentPanel.add(new JLabel("ID:"));
		idFieldDelete = new JTextField();
		contentPanel.add(idFieldDelete);
		
		deletePanel.add(contentPanel, BorderLayout.CENTER);

		JButton deleteButton = new JButton("DELETE");
		deleteButton.setMaximumSize(new Dimension(150, 30));
		deleteButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				Product product = mainGUI.getController().getProductBLL().findById(Integer.parseInt(idFieldDelete.getText().toString()));
				System.out.println(product);
				if (product == null)	
				{
					JOptionPane.showMessageDialog(null, "Client with id " + idFieldDelete.getText().toString() + " does not exist!!", "WARNING", JOptionPane.INFORMATION_MESSAGE);
				}
				else {
					mainGUI.getController().getProductBLL().delete(product);
					JOptionPane.showMessageDialog(null, "DELETE SUCCESSFUL", "WARNING", JOptionPane.INFORMATION_MESSAGE);
				}
				
			}
		});

		deletePanel.add(deleteButton, BorderLayout.SOUTH);

		// Hide the panel initially
		deletePanel.setVisible(true);

		// add(deletePanel, BorderLayout.CENTER); // Add the add client panel to the
		// bottom of the main panel
	}
}
