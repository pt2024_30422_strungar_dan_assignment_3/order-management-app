package org.ordermanagement.presentation;

import javax.swing.*;
import java.awt.event.*;

/**
 * The StartPanel class represents the initial panel displayed when the application starts.
 * It provides buttons to access different management functionalities.
 */
public class StartPanel extends JPanel
{
	private MainGUI mainGUI; // Reference to the MainGUI class

	/**
     * Constructs a new StartPanel object.
     *
     * @param mainGUI The reference to the MainGUI.
     */
	public StartPanel(MainGUI mainGUI)
	{
		this.mainGUI = mainGUI; // Store reference to the MainGUI
		
		JButton btnCreateClient = new JButton("Client Manager");
		btnCreateClient.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				mainGUI.showClientPanel(); // Call showClientPanel() from MainGUI
			}
		});
		add(btnCreateClient);
		
		JButton btnCreateProduct = new JButton("Product Manager");
		btnCreateProduct.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				mainGUI.showProductPanel(); // Call showClientPanel() from MainGUI
			}
		});
		add(btnCreateProduct);
		
		JButton btnCreateOrder = new JButton("Order Manager");
		btnCreateOrder.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				mainGUI.showOrderPanel(); // Call showClientPanel() from MainGUI
			}
		});
		add(btnCreateOrder);
		
		JButton btnBill = new JButton("View Bills");
		btnBill.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				mainGUI.showBillPanel(); // Call showClientPanel() from MainGUI
			}
		});
		add(btnBill);
		
	}
}
