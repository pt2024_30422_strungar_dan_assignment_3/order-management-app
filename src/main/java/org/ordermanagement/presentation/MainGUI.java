package org.ordermanagement.presentation;

import javax.swing.*;
import java.awt.*;

/**
 * The MainGUI class represents the main graphical user interface of the Order Management App.
 * It manages different panels and controls the navigation between them.
 */
public class MainGUI extends JFrame {

    private StartPanel startPanel;
    private ClientPanel clientPanel;
    private ProductPanel productPanel;
    private OrderPanel orderPanel;
    private BillPanel billPanel;
    private Controller controller = new Controller();

    /**
     * Constructs a new MainGUI object.
     */
    public MainGUI() {
        setTitle("Order Management App");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null); // Center the window
        setSize(1000, 600);
        // Initialize panels
        startPanel = new StartPanel(this);
        clientPanel = new ClientPanel(this);
        productPanel = new ProductPanel(this);
        orderPanel = new OrderPanel(this);
        billPanel = new BillPanel(this);

        // Add start panel to frame initially
        add(startPanel);
        setVisible(true);
    }

    /**
     * Shows the client panel.
     */
    public void showClientPanel() {
        // Replace start panel with client panel in the frame
        getContentPane().removeAll();
        add(clientPanel);
        revalidate();
        repaint();
    }

    /**
     * Shows the product panel.
     */
    public void showProductPanel() {
        // Replace start panel with product panel in the frame
        getContentPane().removeAll();
        add(productPanel);
        revalidate();
        repaint();
    }

    /**
     * Shows the order panel.
     */
    public void showOrderPanel() {
        // Replace start panel with order panel in the frame
        getContentPane().removeAll();
        add(orderPanel);
        revalidate();
        repaint();
    }

    /**
     * Shows the start panel.
     */
    public void showStartPanel() {
        // Replace client panel with start panel in the frame
        getContentPane().removeAll();
        add(startPanel);
        revalidate();
        repaint();
    }

    /**
     * Shows the bill panel.
     */
    public void showBillPanel() {
        // Replace client panel with bill panel in the frame
        getContentPane().removeAll();
        add(billPanel);
        revalidate();
        repaint();
    }

    /**
     * Gets the controller associated with this MainGUI.
     *
     * @return The controller.
     */
    public Controller getController() {
        return this.controller;
    }

}
