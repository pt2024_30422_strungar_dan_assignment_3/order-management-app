package org.ordermanagement.presentation;

import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableModel;

import org.ordermanagement.business_logic.ClientBLL;
import org.ordermanagement.model.Client;

import java.awt.*;
import java.awt.event.*;
import java.util.EventObject;
import java.util.List;

/**
 * The ClientPanel class represents a graphical user interface panel
 * for managing client information. It allows users to add, update, delete,
 * and list all clients in the system.
 */
public class ClientPanel extends JPanel
{

	private MainGUI mainGUI; // Reference to the MainGUI class
	private JPanel utilPanel = new JPanel();
	private JTable table;
	private DefaultTableModel tableModel;
	private JPanel addClientPanel, updatePanel, deletePanel, listAllPanel;
	private JTextField idFieldInsert, cnpFieldInsert, nameFieldInsert, addressFieldInsert, emailFieldInsert;
	private JTextField idFieldUpdate, cnpFieldUpdate, nameFieldUpdate, addressFieldUpdate, emailFieldUpdate;
	private JTextField idFieldDelete, cnpFieldDelete, nameFieldDelete, addressFieldDelete, emailFieldDelete;

	/**
     * Constructs a new ClientPanel with a reference to the MainGUI.
     * 
     * @param mainGUI The MainGUI instance associated with this panel.
     */
	public ClientPanel(MainGUI mainGUI)
	{
		this.mainGUI = mainGUI; // Store reference to the MainGUI

		setLayout(new BorderLayout());

		JPanel titlePanel = new JPanel();
		JLabel titleLabel = new JLabel("CLIENT MANAGER WINDOW");
		titlePanel.add(titleLabel);

		JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS)); // Set layout to BoxLayout with Y_AXIS
																			// alignment

		// Add vertical glue to center the button panel vertically
		leftPanel.add(Box.createVerticalGlue());

		// Create a separate BoxLayout for buttonPanel
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));

		// Create and add 5 buttons to the button panel with rigid areas between them
		JButton createButton = new JButton("ADD NEW");
		createButton.setMaximumSize(new Dimension(150, 30));
		createButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				((CardLayout) utilPanel.getLayout()).show(utilPanel, "ADD_CLIENT_PANEL");
			}
		});
		buttonPanel.add(createButton);
		buttonPanel.add(Box.createRigidArea(new Dimension(0, 20))); // Add 5 pixels of vertical space

		JButton updateButton = new JButton("UPDATE");
		updateButton.setMaximumSize(new Dimension(150, 30));
		updateButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				((CardLayout) utilPanel.getLayout()).show(utilPanel, "UPDATE_CLIENT_PANEL");
			}
		});
		buttonPanel.add(updateButton);
		buttonPanel.add(Box.createRigidArea(new Dimension(0, 20))); // Add 5 pixels of vertical space

		JButton deleteButton = new JButton("DELETE");
		deleteButton.setMaximumSize(new Dimension(150, 30));
		deleteButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				((CardLayout) utilPanel.getLayout()).show(utilPanel, "DELETE_CLIENT_PANEL");
			}
		});
		buttonPanel.add(deleteButton);
		buttonPanel.add(Box.createRigidArea(new Dimension(0, 20))); // Add 5 pixels of vertical space

		JButton listAllButton = new JButton("LIST ALL");
		listAllButton.setMaximumSize(new Dimension(150, 30));
		listAllButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				updateTable();
				((CardLayout) utilPanel.getLayout()).show(utilPanel, "LIST_ALL_PANEL");

			}
		});
		buttonPanel.add(listAllButton);
		buttonPanel.add(Box.createRigidArea(new Dimension(0, 20))); // Add 5 pixels of vertical space

		JButton backButton = new JButton("BACK");
		backButton.setMaximumSize(new Dimension(150, 30));
		backButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				mainGUI.showStartPanel();
			}
		});
		buttonPanel.add(backButton);

		// Add the button panel to the left panel
		leftPanel.add(buttonPanel);

		// Add more vertical glue to center the button panel vertically
		leftPanel.add(Box.createVerticalGlue());

		// Align the button panel in the center horizontally
		buttonPanel.setAlignmentX(Component.CENTER_ALIGNMENT);

		leftPanel.setPreferredSize(new Dimension(300, 100));

		utilPanel.setLayout(new CardLayout());

		JLayeredPane contentPane = new JLayeredPane();

		createListAllPanel();
		utilPanel.add(listAllPanel, "LIST_ALL_PANEL");
		createAddClientPanel();
		utilPanel.add(addClientPanel, "ADD_CLIENT_PANEL");
		createUpdateClientPanel();
		utilPanel.add(updatePanel, "UPDATE_CLIENT_PANEL");
		createDeleteClientPanel();
		utilPanel.add(deletePanel, "DELETE_CLIENT_PANEL");

		utilPanel.add(contentPane);

		add(titlePanel, BorderLayout.NORTH);
		add(leftPanel, BorderLayout.WEST);
		add(utilPanel, BorderLayout.CENTER);

		((CardLayout) utilPanel.getLayout()).show(utilPanel, "LIST_ALL_PANEL");

	}

	private void createListAllPanel()
	{
		listAllPanel = new JPanel();
		listAllPanel.setLayout(new BorderLayout());

		tableModel = new DefaultTableModel();

		String[] columnNames =
		{ "ID", "CNP", "Name", "Address", "Email" };

		for (String columnName : columnNames)
		{
			tableModel.addColumn(columnName);
		}

		table = new JTable(tableModel);

		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane scrollPane = new JScrollPane(table);

		listAllPanel.add(scrollPane, BorderLayout.CENTER);

		JLabel titleLabel = new JLabel("Client List");
		listAllPanel.add(titleLabel, BorderLayout.NORTH);
		updateTable();
	}

	private void updateTable()
	{
		List<Client> clients = mainGUI.getController().getClientBLL().findAll();
		if (tableModel.getRowCount() > 0)
		{
			for (int i = tableModel.getRowCount() - 1; i >= 0; i--)
			{
				tableModel.removeRow(i);
			}
		}
		for (Client client : clients)
		{
			System.out.println(client);
			Object[] row =
			{ client.getClient_id(), client.getCNP(), client.getClient_name(), client.getClient_adress(),
					client.getClient_email() };
			tableModel.addRow(row);
		}

	}

	private void createAddClientPanel()
	{
		addClientPanel = new JPanel();

		addClientPanel.setLayout(new BorderLayout());

		JLabel titleLabel = new JLabel("INSERT WINDOW");
		addClientPanel.add(titleLabel, BorderLayout.NORTH);

		JPanel contentPanel = new JPanel();
		contentPanel.setLayout(new GridLayout(4, 2));
		contentPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		contentPanel.add(new JLabel("CNP:"));
		cnpFieldInsert = new JTextField();
		contentPanel.add(cnpFieldInsert);

		contentPanel.add(new JLabel("Name:"));
		nameFieldInsert = new JTextField();
		contentPanel.add(nameFieldInsert);

		contentPanel.add(new JLabel("Email:"));
		emailFieldInsert = new JTextField();
		contentPanel.add(emailFieldInsert);

		contentPanel.add(new JLabel("Address:"));
		addressFieldInsert = new JTextField();
		contentPanel.add(addressFieldInsert);

		addClientPanel.add(contentPanel, BorderLayout.CENTER);

		JButton insertButton = new JButton("INSERT");
		insertButton.setMaximumSize(new Dimension(150, 30));
		insertButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				System.out.println(cnpFieldInsert.getText().toString());
				System.out.println(nameFieldInsert.getText().toString());
				System.out.println(emailFieldInsert.getText().toString());
				System.out.println(addressFieldInsert.getText().toString());
				mainGUI.getController().getClientBLL()
						.insert(new Client(cnpFieldInsert.getText().toString(), nameFieldInsert.getText().toString(),
								emailFieldInsert.getText().toString(), addressFieldInsert.getText().toString()));
			}
		});

		addClientPanel.add(insertButton, BorderLayout.SOUTH);

		// Hide the panel initially
		addClientPanel.setVisible(true);

		// add(addClientPanel, BorderLayout.CENTER); // Add the add client panel to the
		// bottom of the main panel
	}

	private void createUpdateClientPanel()
	{
		updatePanel = new JPanel();

		updatePanel.setLayout(new BorderLayout());

		JLabel titleLabel = new JLabel("UPDATE WINDOW");
		updatePanel.add(titleLabel, BorderLayout.NORTH);

		JPanel contentPanel = new JPanel();
		contentPanel.setLayout(new GridLayout(5, 2));
		contentPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		contentPanel.add(new JLabel("ID:"));
		idFieldUpdate = new JTextField();
		contentPanel.add(idFieldUpdate);

		contentPanel.add(new JLabel("CNP:"));
		cnpFieldUpdate = new JTextField();
		contentPanel.add(cnpFieldUpdate);

		contentPanel.add(new JLabel("Name:"));
		nameFieldUpdate = new JTextField();
		contentPanel.add(nameFieldUpdate);

		contentPanel.add(new JLabel("Email:"));
		emailFieldUpdate = new JTextField();
		contentPanel.add(emailFieldUpdate);

		contentPanel.add(new JLabel("Address:"));
		addressFieldUpdate = new JTextField();
		contentPanel.add(addressFieldUpdate);

		updatePanel.add(contentPanel, BorderLayout.CENTER);

		JButton updateButton = new JButton("UPDATE");
		updateButton.setMaximumSize(new Dimension(150, 30));
		updateButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				System.out.println(idFieldUpdate.getText().toString());
				System.out.println(nameFieldUpdate.getText().toString());
				System.out.println(emailFieldUpdate.getText().toString());
				System.out.println(addressFieldUpdate.getText().toString());
				Client client = mainGUI.getController().getClientBLL().findById(Integer.parseInt(idFieldUpdate.getText().toString()));
				System.out.println(client);
				
				if (client == null)	
				{
					JOptionPane.showMessageDialog(null, "Client with id " + idFieldUpdate.getText().toString() + " does not exist!!", "WARNING", JOptionPane.INFORMATION_MESSAGE);
				}
				else {
					client.setCNP(cnpFieldUpdate.getText().toString());
					client.setClient_name(nameFieldUpdate.getText().toString());
					client.setClient_email(emailFieldUpdate.getText().toString());
					client.setClient_adress(addressFieldUpdate.getText().toString());
					mainGUI.getController().getClientBLL().update(client);
					JOptionPane.showMessageDialog(null, "UPDATE SUCCESSFUL", "WARNING", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});

		updatePanel.add(updateButton, BorderLayout.SOUTH);

		// Hide the panel initially
		updatePanel.setVisible(true);

		// add(updatePanel, BorderLayout.CENTER); // Add the add client panel to the
		// bottom of the main panel
	}

	private void createDeleteClientPanel()
	{
		deletePanel = new JPanel();
		
		deletePanel.setLayout(new BorderLayout()); 
		
		JLabel titleLabel = new JLabel("DELETE WINDOW");
		deletePanel.add(titleLabel, BorderLayout.NORTH);

		JPanel contentPanel = new JPanel();
		contentPanel.setLayout(new GridLayout(1, 2));
		contentPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		contentPanel.add(new JLabel("ID:"));
		idFieldDelete = new JTextField();
		contentPanel.add(idFieldDelete);
		
		deletePanel.add(contentPanel, BorderLayout.CENTER);

		JButton deleteButton = new JButton("DELETE");
		deleteButton.setMaximumSize(new Dimension(150, 30));
		deleteButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				Client client = mainGUI.getController().getClientBLL().findById(Integer.parseInt(idFieldDelete.getText().toString()));
				System.out.println(client);
				if (client == null)	
				{
					JOptionPane.showMessageDialog(null, "Client with id " + idFieldDelete.getText().toString() + " does not exist!!", "WARNING", JOptionPane.INFORMATION_MESSAGE);
				}
				else {
					mainGUI.getController().getClientBLL().delete(client);
					JOptionPane.showMessageDialog(null, "DELETE SUCCESSFUL", "WARNING", JOptionPane.INFORMATION_MESSAGE);
				}
				
			}
		});

		deletePanel.add(deleteButton, BorderLayout.SOUTH);

		// Hide the panel initially
		deletePanel.setVisible(true);

		// add(deletePanel, BorderLayout.CENTER); // Add the add client panel to the
		// bottom of the main panel
	}
}
