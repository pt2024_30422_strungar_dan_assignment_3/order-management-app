package org.ordermanagement.presentation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.*;

import org.ordermanagement.model.Bill;

/**
 * The BillPanel class represents a panel for displaying bill information in the GUI.
 */
public class BillPanel extends JPanel {
    private MainGUI mainGUI;

    /**
     * Constructs a BillPanel with a reference to the MainGUI.
     * 
     * @param mainGUI the MainGUI instance
     */
    public BillPanel(MainGUI mainGUI) {
        this.mainGUI = mainGUI; // Store reference to the MainGUI
        setLayout(new BorderLayout());
        JPanel contentPanel = new JPanel();
        contentPanel.setLayout(new GridLayout(0, 4, 10, 10));

        // Retrieve bill information from the controller
        List<Bill> bills = mainGUI.getController().getBillBLL().findAll();

        // Display each bill information in the panel
        for (Bill bill : bills) {
            JLabel orderIDLabel = new JLabel(bill.order_id().toString());
            JLabel clientNameLabel = new JLabel(bill.client_name());
            JLabel productListLabel = new JLabel(bill.product_list());
            JLabel totalPriceLabel = new JLabel(Double.valueOf(bill.total_price()).toString());

            contentPanel.add(orderIDLabel);
            contentPanel.add(clientNameLabel);
            contentPanel.add(productListLabel);
            contentPanel.add(totalPriceLabel);
        }

        add(contentPanel, BorderLayout.CENTER);

        add(new JLabel("Bill list:"), BorderLayout.NORTH);

        // Add a button to navigate back to the start panel
        JButton backButton = new JButton("BACK");
        backButton.setMaximumSize(new Dimension(150, 30));
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mainGUI.showStartPanel();
            }
        });
        add(backButton, BorderLayout.SOUTH);
    }
}
