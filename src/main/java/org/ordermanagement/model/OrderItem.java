package org.ordermanagement.model;

/**
 * The OrderItem class represents an item in an order with its attributes.
 */
public class OrderItem {

    /** The unique identifier for the order. */
    public Integer order_id;

    /** The unique identifier for the product associated with the item. */
    public Integer product_id;

    /** The quantity of the product in the order item. */
    private int quantity;

    /** Constructs an empty OrderItem object. */
    public OrderItem() {}

    /**
     * Constructs an OrderItem object with the specified attributes.
     * 
     * @param order_id   the unique identifier for the order
     * @param product_id the unique identifier for the product associated with the item
     * @param quantity   the quantity of the product in the order item
     */
    public OrderItem(Integer order_id, Integer product_id, int quantity) {
        this.order_id = order_id;
        this.product_id = product_id;
        this.quantity = quantity;
    }

    /**
     * Retrieves the order ID associated with the order item.
     * 
     * @return the order ID
     */
    public Integer getOrder_id() {
        return order_id;
    }

    /**
     * Sets the order ID associated with the order item.
     * 
     * @param order_id the order ID to set
     */
    public void setOrder_id(Integer order_id) {
        this.order_id = order_id;
    }

    /**
     * Retrieves the product ID associated with the order item.
     * 
     * @return the product ID
     */
    public Integer getProduct_id() {
        return product_id;
    }

    /**
     * Sets the product ID associated with the order item.
     * 
     * @param product_id the product ID to set
     */
    public void setProduct_id(Integer product_id) {
        this.product_id = product_id;
    }

    /**
     * Retrieves the quantity of the product in the order item.
     * 
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Sets the quantity of the product in the order item.
     * 
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "OrderItem [order_id=" + order_id + ", product_id=" + product_id + ", quantity=" + quantity + "]";
    }

}
