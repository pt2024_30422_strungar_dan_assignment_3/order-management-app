package org.ordermanagement.model;

/**
 * The Product class represents a product with its attributes.
 */
public class Product {

    /** The unique identifier for the product. */
    public Integer product_id;

    /** The name of the product. */
    private String product_name;

    /** The quantity of the product in stock. */
    private int stock_quantity;

    /** The price of the product. */
    private double price;

    /** Constructs an empty Product object. */
    public Product() {}

    /**
     * Constructs a Product object with the specified attributes.
     * 
     * @param product_name    the name of the product
     * @param stock_quantity  the quantity of the product in stock
     * @param price           the price of the product
     */
    public Product(String product_name, int stock_quantity, double price) {
        this.product_name = product_name;
        this.stock_quantity = stock_quantity;
        this.price = price;
    }

    /**
     * Constructs a Product object with the specified attributes.
     * 
     * @param product_id      the unique identifier for the product
     * @param product_name    the name of the product
     * @param stock_quantity  the quantity of the product in stock
     * @param price           the price of the product
     */
    public Product(Integer product_id, String product_name, int stock_quantity, double price) {
        this.product_id = product_id;
        this.product_name = product_name;
        this.stock_quantity = stock_quantity;
        this.price = price;
    }

    /**
     * Retrieves the product ID.
     * 
     * @return the product ID
     */
    public Integer getProduct_id() {
        return product_id;
    }

    /**
     * Sets the product ID.
     * 
     * @param product_id the product ID to set
     */
    public void setProduct_id(Integer product_id) {
        this.product_id = product_id;
    }

    /**
     * Retrieves the name of the product.
     * 
     * @return the name of the product
     */
    public String getProduct_name() {
        return product_name;
    }

    /**
     * Sets the name of the product.
     * 
     * @param product_name the name of the product to set
     */
    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    /**
     * Retrieves the quantity of the product in stock.
     * 
     * @return the stock quantity
     */
    public int getStock_quantity() {
        return stock_quantity;
    }

    /**
     * Sets the quantity of the product in stock.
     * 
     * @param stock_quantity the stock quantity to set
     */
    public void setStock_quantity(int stock_quantity) {
        this.stock_quantity = stock_quantity;
    }

    /**
     * Retrieves the price of the product.
     * 
     * @return the price of the product
     */
    public double getPrice() {
        return price;
    }

    /**
     * Sets the price of the product.
     * 
     * @param price the price of the product to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product [product_id=" + product_id + ", product_name=" + product_name + ", stock_quantity="
                + stock_quantity + ", price=" + price + "]";
    }

}
