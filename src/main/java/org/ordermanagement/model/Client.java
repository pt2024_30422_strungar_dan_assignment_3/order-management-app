package org.ordermanagement.model;

/**
 * The Client class represents a client entity with its attributes.
 */
public class Client {

    /** The unique identifier for the client. */
    public Integer client_id;

    /** The CNP (Personal Numerical Code) of the client. */
    private String CNP;

    /** The name of the client. */
    private String client_name;

    /** The email address of the client. */
    private String client_email;

    /** The address of the client. */
    private String client_adress;

    /** Constructs an empty Client object. */
    public Client() {}

    /**
     * Constructs a Client object with the specified attributes.
     * 
     * @param CNP           the CNP (Personal Numerical Code) of the client
     * @param client_name   the name of the client
     * @param client_email  the email address of the client
     * @param client_adress the address of the client
     */
    public Client(String CNP, String client_name, String client_email, String client_adress) {
        this.CNP = CNP;
        this.client_name = client_name;
        this.client_email = client_email;
        this.client_adress = client_adress;
    }

    /**
     * Constructs a Client object with the specified attributes.
     * 
     * @param client_id     the unique identifier for the client
     * @param CNP           the CNP (Personal Numerical Code) of the client
     * @param client_name   the name of the client
     * @param client_email  the email address of the client
     * @param client_adress the address of the client
     */
    public Client(Integer client_id, String CNP, String client_name, String client_email, String client_adress) {
        this.client_id = client_id;
        this.CNP = CNP;
        this.client_name = client_name;
        this.client_email = client_email;
        this.client_adress = client_adress;
    }

    /**
     * Retrieves the client ID.
     * 
     * @return the client ID
     */
    public Integer getClient_id() {
        return client_id;
    }

    /**
     * Sets the client ID.
     * 
     * @param client_id the client ID to set
     */
    public void setClient_id(Integer client_id) {
        this.client_id = client_id;
    }

    /**
     * Retrieves the CNP (Personal Numerical Code) of the client.
     * 
     * @return the CNP of the client
     */
    public String getCNP() {
        return CNP;
    }

    /**
     * Sets the CNP (Personal Numerical Code) of the client.
     * 
     * @param cNP the CNP to set
     */
    public void setCNP(String cNP) {
        CNP = cNP;
    }

    /**
     * Retrieves the name of the client.
     * 
     * @return the name of the client
     */
    public String getClient_name() {
        return client_name;
    }

    /**
     * Sets the name of the client.
     * 
     * @param client_name the name of the client to set
     */
    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    /**
     * Retrieves the email address of the client.
     * 
     * @return the email address of the client
     */
    public String getClient_email() {
        return client_email;
    }

    /**
     * Sets the email address of the client.
     * 
     * @param client_email the email address of the client to set
     */
    public void setClient_email(String client_email) {
        this.client_email = client_email;
    }

    /**
     * Retrieves the address of the client.
     * 
     * @return the address of the client
     */
    public String getClient_adress() {
        return client_adress;
    }

    /**
     * Sets the address of the client.
     * 
     * @param client_adress the address of the client to set
     */
    public void setClient_adress(String client_adress) {
        this.client_adress = client_adress;
    }

    @Override
    public String toString() {
        return "Client [clientId=" + client_id + ", CNP=" + CNP + ", clientName=" + client_name + ", client_email="
                + client_email + ", client_adress=" + client_adress + "]";
    }

}
