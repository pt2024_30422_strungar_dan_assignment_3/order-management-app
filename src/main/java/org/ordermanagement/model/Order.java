package org.ordermanagement.model;

import java.time.LocalDateTime;

/**
 * The Order class represents an order entity with its attributes.
 */
public class Order {

    /** The unique identifier for the order. */
    private Integer order_id;

    /** The unique identifier for the client associated with the order. */
    private Integer client_id;

    /** The date and time when the order was placed. */
    private LocalDateTime order_date;

    /** Constructs an empty Order object. */
    public Order() {}

    /**
     * Constructs an Order object with the specified attributes.
     * 
     * @param client_id  the unique identifier for the client associated with the order
     * @param order_date the date and time when the order was placed
     */
    public Order(Integer client_id, LocalDateTime order_date) {
        this.client_id = client_id;
        this.order_date = order_date;
    }

    /**
     * Constructs an Order object with the specified attributes.
     * 
     * @param order_id   the unique identifier for the order
     * @param client_id  the unique identifier for the client associated with the order
     * @param order_date the date and time when the order was placed
     */
    public Order(Integer order_id, Integer client_id, LocalDateTime order_date) {
        this.order_id = order_id;
        this.client_id = client_id;
        this.order_date = order_date;
    }

    /**
     * Retrieves the order ID.
     * 
     * @return the order ID
     */
    public Integer getOrder_id() {
        return order_id;
    }

    /**
     * Sets the order ID.
     * 
     * @param order_id the order ID to set
     */
    public void setOrder_id(Integer order_id) {
        this.order_id = order_id;
    }

    /**
     * Retrieves the client ID associated with the order.
     * 
     * @return the client ID
     */
    public Integer getClient_id() {
        return client_id;
    }

    /**
     * Sets the client ID associated with the order.
     * 
     * @param client_id the client ID to set
     */
    public void setClient_id(Integer client_id) {
        this.client_id = client_id;
    }

    /**
     * Retrieves the date and time when the order was placed.
     * 
     * @return the order date and time
     */
    public LocalDateTime getOrder_date() {
        return order_date;
    }

    /**
     * Sets the date and time when the order was placed.
     * 
     * @param order_date the order date and time to set
     */
    public void setOrder_date(LocalDateTime order_date) {
        this.order_date = order_date;
    }

    @Override
    public String toString() {
        return "Order [order_id=" + order_id + ", client_id=" + client_id + ", order_date=" + order_date + "]";
    }

}
