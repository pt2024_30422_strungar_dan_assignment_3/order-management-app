package org.ordermanagement.model;

/**
 * The Bill class represents a bill associated with an order.
 * It contains information such as the order ID, client name, product list, and total price.
 */
public record Bill(Integer order_id, String client_name, String product_list, double total_price) {
    
}
