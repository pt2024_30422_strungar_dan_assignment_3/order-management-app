package org.ordermanagement.business_logic;

import java.util.List;
import org.ordermanagement.data_access.OrderItemDAO;
import org.ordermanagement.model.OrderItem;

/**
 * The OrderItemBLL class provides business logic related to OrderItems.
 */
public class OrderItemBLL {
    private OrderItemDAO orderItemDAO;
    
    /**
     * Constructs a new OrderItemBLL instance and initializes the OrderItemDAO.
     */
    public OrderItemBLL() {
        this.orderItemDAO = OrderItemDAO.getInstance();
    }

    /**
     * Retrieves all order items from the data source.
     *
     * @return a list of all order items.
     */
    public List<OrderItem> findAll() {
        return orderItemDAO.findAll();
    }
    
    /**
     * Retrieves order items by their unique ID.
     *
     * @param id the ID of the order items to retrieve.
     * @return a list of order items with the specified ID.
     */
    public List<OrderItem> findById(Integer id) {
        return orderItemDAO.findById(id);
    }
    
    /**
     * Inserts a new order item into the data source.
     *
     * @param orderItem the order item to insert.
     * @return the inserted order item.
     */
    public OrderItem insert(OrderItem orderItem) {
        orderItemDAO.insert(orderItem);
        return orderItem;
    }
    
    /**
     * Updates an existing order item in the data source.
     *
     * @param orderItem the order item to update.
     * @return the updated order item.
     */
    public OrderItem update(OrderItem orderItem) {
        orderItemDAO.update(orderItem);
        return orderItem;
    }
    
    /**
     * Deletes an order item from the data source.
     *
     * @param orderItem the order item to delete.
     * @return the deleted order item.
     */
    public OrderItem delete(OrderItem orderItem) {
        orderItemDAO.delete(orderItem);
        return orderItem;
    }
}
