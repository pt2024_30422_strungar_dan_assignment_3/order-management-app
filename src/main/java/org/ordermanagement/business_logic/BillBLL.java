package org.ordermanagement.business_logic;

import java.util.ArrayList;
import java.util.List;

import org.ordermanagement.data_access.BillDAO;
import org.ordermanagement.model.Bill;

/**
 * Business Logic Layer for handling operations related to Bills.
 */
public class BillBLL {
    private BillDAO billDAO;

    /**
     * Constructs a new BillBLL and initializes the BillDAO instance.
     */
    public BillBLL() {
        billDAO = BillDAO.getInstance();
    }

    /**
     * Retrieves all bills from the data source.
     * 
     * @return a list of all bills.
     */
    public List<Bill> findAll() {
        List<Bill> list = new ArrayList<>();
        list = billDAO.findAll();
        return list;
    }

    /**
     * Inserts a new bill into the data source.
     * 
     * @param bill the bill to be inserted.
     * @return the inserted bill.
     */
    public Bill insert(Bill bill) {
        billDAO.insert(bill);
        return bill;
    }

    /**
     * Deletes an existing bill from the data source.
     * 
     * @param bill the bill to be deleted.
     * @return the deleted bill.
     */
    public Bill delete(Bill bill) {
        billDAO.delete(bill);
        return bill;
    }
}
