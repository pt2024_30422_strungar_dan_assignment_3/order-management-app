package org.ordermanagement.business_logic;

import java.util.ArrayList;
import java.util.List;

import org.ordermanagement.data_access.ProductDAO;
import org.ordermanagement.model.Product;

/**
 * The ProductBLL class provides business logic related to products.
 */
public class ProductBLL {
    private ProductDAO productDAO;
    
    /**
     * Constructs a new ProductBLL instance and initializes the ProductDAO.
     */
    public ProductBLL() {
        productDAO = ProductDAO.getInstance();
    }
    
    /**
     * Retrieves a product by its unique ID.
     *
     * @param id the ID of the product to retrieve.
     * @return the product with the specified ID, or null if no such product exists.
     */
    public Product findById(Integer id) {
        Product product = productDAO.findById(id).get(0);
        return product;
    }
    
    /**
     * Retrieves all products from the data source.
     *
     * @return a list of all products.
     */
    public List<Product> findAll() {
        List<Product> list = new ArrayList<>();
        list = productDAO.findAll();
        return list;
    }
    
    /**
     * Inserts a new product into the data source.
     *
     * @param product the product to insert.
     * @return the inserted product.
     */
    public Product insert(Product product) {
        productDAO.insert(product);
        return product;
    }
    
    /**
     * Updates an existing product in the data source.
     *
     * @param product the product to update.
     * @return the updated product.
     */
    public Product update(Product product) {
        productDAO.update(product);
        return product;
    }
    
    /**
     * Deletes a product from the data source.
     *
     * @param product the product to delete.
     * @return the deleted product.
     */
    public Product delete(Product product) {
        productDAO.delete(product);
        return product;
    }
}
