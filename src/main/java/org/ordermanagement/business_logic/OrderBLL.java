package org.ordermanagement.business_logic;

import java.util.List;
import org.ordermanagement.data_access.OrderDAO;
import org.ordermanagement.model.Order;

/**
 * The OrderBLL class provides business logic related to Orders.
 */
public class OrderBLL {
    private OrderDAO orderDAO;
    
    /**
     * Constructs a new OrderBLL instance and initializes the OrderDAO.
     */
    public OrderBLL() {
        orderDAO = OrderDAO.getInstance();
    }
    
    /**
     * Retrieves all orders from the data source.
     *
     * @return a list of all orders.
     */
    public List<Order> findAll() {
        return orderDAO.findAll();
    }
    
    /**
     * Retrieves an order by its unique ID.
     *
     * @param id the ID of the order to retrieve.
     * @return the order with the specified ID, or null if no such order exists.
     */
    public Order findById(Integer id) {
        if (orderDAO.findById(id) == null || orderDAO.findById(id).size() == 0) return null;
        return orderDAO.findById(id).get(0);
    }
    
    /**
     * Inserts a new order into the data source.
     *
     * @param order the order to insert.
     * @return the inserted order.
     */
    public Order insert(Order order) {
        orderDAO.insert(order);
        return order;
    }
    
    /**
     * Updates an existing order in the data source.
     *
     * @param order the order to update.
     * @return the updated order.
     */
    public Order update(Order order) {
        orderDAO.update(order);
        return order;
    }
    
    /**
     * Deletes an order from the data source.
     *
     * @param order the order to delete.
     * @return the deleted order.
     */
    public Order delete(Order order) {
        orderDAO.delete(order);
        return order;
    }
}
