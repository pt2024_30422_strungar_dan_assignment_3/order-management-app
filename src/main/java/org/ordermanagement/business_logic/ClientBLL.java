package org.ordermanagement.business_logic;

import java.util.ArrayList;
import java.util.List;

import org.ordermanagement.data_access.ClientDAO;
import org.ordermanagement.model.Client;

/**
 * Business Logic Layer for handling operations related to Clients.
 */
public class ClientBLL {
    private ClientDAO clientDAO;

    /**
     * Constructs a new ClientBLL and initializes the ClientDAO instance.
     */
    public ClientBLL() {
        clientDAO = ClientDAO.getInstance();
    }

    /**
     * Retrieves all clients from the data source.
     * 
     * @return a list of all clients.
     */
    public List<Client> findAll() {
        List<Client> list = new ArrayList<>();
        list = clientDAO.findAll();
        return list;
    }

    /**
     * Finds a client by their ID.
     * 
     * @param id the ID of the client to be found.
     * @return the client with the specified ID, or null if no such client exists.
     */
    public Client findById(Integer id) {
        if (clientDAO.findById(id).size() == 0) {
            System.err.println("Client with id: " + id + " does not exist");
            return null;
        }
        return clientDAO.findById(id).getFirst();
    }

    /**
     * Finds clients by their name.
     * 
     * @param name the name of the clients to be found.
     * @return a list of clients with the specified name, or null if no such clients exist.
     */
    public List<Client> findByName(String name) {
        if (clientDAO.findByName(name).size() == 0) {
            System.err.println("Client with name: " + name + " does not exist");
            return null;
        }
        return clientDAO.findByName(name);
    }

    /**
     * Finds a client by their CNP (Unique Identifier).
     * 
     * @param CNP the CNP of the client to be found.
     * @return the client with the specified CNP, or null if no such client exists.
     */
    public Client findByCNP(String CNP) {
        if (clientDAO.findByCNP(CNP).size() == 0) {
            System.err.println("Client with CNP: " + CNP + " does not exist");
            return null;
        }
        return clientDAO.findByCNP(CNP).getFirst();
    }

    /**
     * Inserts a new client into the data source.
     * 
     * @param client the client to be inserted.
     * @return the inserted client.
     */
    public Client insert(Client client) {
        clientDAO.insert(client);
        return client;
    }

    /**
     * Updates an existing client in the data source.
     * 
     * @param client the client to be updated.
     * @return the updated client.
     */
    public Client update(Client client) {
        clientDAO.update(client);
        return client;
    }

    /**
     * Deletes an existing client from the data source.
     * 
     * @param client the client to be deleted.
     * @return the deleted client.
     */
    public Client delete(Client client) {
        clientDAO.delete(client);
        return client;
    }
}
