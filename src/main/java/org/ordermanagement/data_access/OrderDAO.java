package org.ordermanagement.data_access;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;

import org.ordermanagement.connection.ConnectionFactory;
import org.ordermanagement.model.Order;
/**
 * The OrderDAO class provides methods to access and manipulate Order objects in the database.
 */
public class OrderDAO extends AbstractDAO<Order>
{
	private static OrderDAO INSTANCE;
	
	private OrderDAO() {}
	/**
     * Retrieves the singleton instance of OrderDAO.
     *
     * @return the singleton instance of OrderDAO.
     */
	public static OrderDAO getInstance() {
		if(INSTANCE == null) {
			INSTANCE = new OrderDAO();
		}
		return INSTANCE;
	}
	
	@Override
	protected String createSelectAllQuery()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM \"order\"");
		System.out.println(sb);
		return sb.toString();
	}
	
	@Override
	protected String createInsertQuery() {
	    StringBuilder sb = new StringBuilder();
	    sb.append("INSERT INTO \"order\" (\"client_id\", \"order_date\") VALUES (?, ?)");
	    return sb.toString();
	}
	
	/**
     * Inserts a new order into the database.
     *
     * @param order the order to insert.
     * @return the inserted order.
     */
	@Override
	public Order insert(Order order) {
	    Connection connection = null;
	    PreparedStatement statement = null;
	    ResultSet generatedKeys = null;
	    String query = createInsertQuery();
	    
	    try {
	        connection = ConnectionFactory.getConnection();
	        statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

	        statement.setObject(1, order.getClient_id());
	        statement.setObject(2, order.getOrder_date());

	        statement.executeUpdate();
	        
	        generatedKeys = statement.getGeneratedKeys();
	        if (generatedKeys.next()) {
                int id = generatedKeys.getInt(1);
                order.setOrder_id(id); // Assuming your Order class has a setId method
            } else {
                throw new SQLException("Creating order failed, no ID obtained.");
            }
	        
	        System.out.println("Insert successful");
	        return order;
	    } catch (SQLException e) {
	        LOGGER.log(Level.WARNING, "OrderDAO:insert " + e.getMessage());
	    } finally {
	        ConnectionFactory.close(statement);
	        ConnectionFactory.close(connection);
	    }
	    return null;
	}
	
}
