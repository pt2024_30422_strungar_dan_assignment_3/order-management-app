package org.ordermanagement.data_access;

import org.ordermanagement.model.Product;

/**
 * The ProductDAO class provides methods to access and manipulate Product objects in the database.
 */
public class ProductDAO extends AbstractDAO<Product> {
    private static ProductDAO INSTANCE;
    
    private ProductDAO() {}
    
    /**
     * Retrieves the singleton instance of ProductDAO.
     *
     * @return the singleton instance of ProductDAO.
     */
    public static ProductDAO getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ProductDAO();
        }
        return INSTANCE;
    }
}
