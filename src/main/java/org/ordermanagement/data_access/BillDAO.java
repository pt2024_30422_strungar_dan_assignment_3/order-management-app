package org.ordermanagement.data_access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import org.ordermanagement.connection.ConnectionFactory;
import org.ordermanagement.model.Bill;
import org.ordermanagement.model.OrderItem;

/**
 * The BillDAO class provides methods to access and manipulate Bill objects in the database.
 */
public class BillDAO extends AbstractDAO<Bill>
{
	private static BillDAO INSTANCE;

	private BillDAO()
	{
	}
	
	 /**
     * Retrieves the singleton instance of BillDAO.
     *
     * @return the singleton instance of BillDAO.
     */
	public static BillDAO getInstance()
	{
		if (INSTANCE == null)
		{
			INSTANCE = new BillDAO();
		}
		return INSTANCE;
	}

	@Override
	protected String createInsertQuery()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO logtable (order_id, client_name, product_list, total_price) VALUES (?, ?, ?, ?)");
		return sb.toString();
	}

	@Override
	protected String createDeleteQuery()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE FROM logtable WHERE order_id = ?");
		System.out.println(sb);
		return sb.toString();
	}

	@Override
	protected String createSelectAllQuery()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM logtable");
		System.out.println(sb);
		return sb.toString();
	}

	@Override
	public Bill insert(Bill bill)
	{
		Connection connection = null;
		PreparedStatement statement = null;
		String query = createInsertQuery();

		try
		{
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);

			statement.setObject(1, bill.order_id());
			statement.setObject(2, bill.client_name());
			statement.setObject(3, bill.product_list());
			statement.setObject(4, bill.total_price());
			statement.executeUpdate();
			System.out.println("Insert successful");
			return bill;
		} catch (SQLException e)
		{
			LOGGER.log(Level.WARNING, "OrderDAO:insert " + e.getMessage());
		} finally
		{
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	@Override
	public boolean delete(Bill bill)
	{
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createDeleteQuery();
		try
		{
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			try
			{
				statement.setObject(1, bill.order_id());

			} catch (Exception e)
			{
				LOGGER.log(Level.WARNING, "Error setting parameter for field: order_id");
				return false;
			}
			statement.executeUpdate();
			int rowsAffected = statement.executeUpdate();
			if (rowsAffected > 0)
			{
				System.out.println("Delete successful");
				return true; // Return true indicating success
			} else
			{
				System.out.println("No records deleted");
				return false; // Return false indicating failure
			}
		} catch (Exception e)
		{
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally
		{
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return false;
	}
}
