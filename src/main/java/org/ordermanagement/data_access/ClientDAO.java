package org.ordermanagement.data_access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import org.ordermanagement.connection.ConnectionFactory;
import org.ordermanagement.model.Client;
/**
 * The ClientDAO class provides methods to access and manipulate Client objects in the database.
 */
public class ClientDAO extends AbstractDAO<Client>
{
	
	private static ClientDAO INSTANCE;
	
	private ClientDAO() {}
	/**
     * Retrieves the singleton instance of ClientDAO.
     *
     * @return the singleton instance of ClientDAO.
     */
	public static ClientDAO getInstance() {
		if(INSTANCE == null) {
			INSTANCE = new ClientDAO();
		}
		return INSTANCE;
	}
	
	/**
     * Retrieves clients by their name.
     *
     * @param name the name of the client to search for.
     * @return a list of clients with the specified name, or null if none found.
     */
	public List<Client> findByName(String name)
	{
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("client_name");
		try
		{
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setObject(1, name);
			resultSet = statement.executeQuery();
			return createObjects(resultSet) ;
		} catch (SQLException e)
		{
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findByName " + e.getMessage());
		} finally
		{
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}
	
	 /**
     * Retrieves clients by their CNP (National Personal Code).
     *
     * @param CNP the CNP of the client to search for.
     * @return a list of clients with the specified CNP, or null if none found.
     */
	public List<Client> findByCNP(String CNP)
	{
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("client_name");
		try
		{
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setObject(1, CNP);
			resultSet = statement.executeQuery();
			return createObjects(resultSet) ;
		} catch (SQLException e)
		{
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findByCNP " + e.getMessage());
		} finally
		{
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}
}
