package org.ordermanagement.data_access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.ordermanagement.connection.ConnectionFactory;
import org.ordermanagement.model.Order;
import org.ordermanagement.model.OrderItem;
/**
 * The OrderItemDAO class provides methods to access and manipulate OrderItem objects in the database.
 */
public class OrderItemDAO extends AbstractDAO<OrderItem>
{
	
	private static OrderItemDAO INSTANCE;
	
	private OrderItemDAO() {}
	/**
     * Retrieves the singleton instance of OrderItemDAO.
     *
     * @return the singleton instance of OrderItemDAO.
     */
	public static OrderItemDAO getInstance() {
		if(INSTANCE == null) {
			INSTANCE = new OrderItemDAO();
		}
		return INSTANCE;
	}
	
	@Override
	protected String createInsertQuery()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO orderitem (order_id, product_id, quantity) VALUES (?, ?, ?)");
		return sb.toString();
	}
	
	@Override
	protected String createUpdateQuery()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE orderitem SET quantity = ? WHERE order_id = ? AND product_id = ?" );
		return sb.toString();
	}
	
	@Override
	protected String createDeleteQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE FROM orderitem WHERE order_id = ? AND product_id = ?");
		System.out.println(sb);
		return sb.toString();
	}
	
	/**
     * Inserts a new order item into the database.
     *
     * @param orderItem the order item to insert.
     * @return the inserted order item.
     */
	@Override
	public OrderItem insert(OrderItem orderItem) {
	    Connection connection = null;
	    PreparedStatement statement = null;
	    String query = createInsertQuery();
	    
	    try {
	        connection = ConnectionFactory.getConnection();
	        statement = connection.prepareStatement(query);

	        statement.setObject(1, orderItem.getOrder_id());
	        statement.setObject(2, orderItem.getProduct_id());
	        statement.setObject(3, orderItem.getQuantity());
	        statement.executeUpdate();
	        System.out.println("Insert successful");
	        return orderItem;
	    } catch (SQLException e) {
	        LOGGER.log(Level.WARNING, "OrderDAO:insert " + e.getMessage());
	    } finally {
	        ConnectionFactory.close(statement);
	        ConnectionFactory.close(connection);
	    }
	    return null;
	}
	
	/**
     * Finds order items by order ID.
     *
     * @param id the ID of the order.
     * @return a list of order items associated with the specified order ID.
     */
	@Override
	public List<OrderItem> findById(Integer id)
	{
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("order_id");
		try
		{
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			return createObjects(resultSet);
		} catch (SQLException e)
		{
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally
		{
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return new ArrayList<>();
	}
	
	/**
     * Finds order items by product ID.
     *
     * @param id the ID of the product.
     * @return a list of order items associated with the specified product ID.
     */
	public List<OrderItem> findByProduct(Integer id)
	{
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("product_id");
		try
		{
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			return createObjects(resultSet);
		} catch (SQLException e)
		{
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally
		{
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return new ArrayList<>();
	}
	
	/**
     * Deletes an order item from the database.
     *
     * @param orderItem the order item to delete.
     * @return true if the deletion was successful, false otherwise.
     */
	@Override
	public boolean delete(OrderItem orderItem)
	{
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createDeleteQuery();
		try
		{
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			try
			{
				statement.setObject(1, orderItem.getOrder_id());
				
			} catch (Exception e)
			{
				LOGGER.log(Level.WARNING, "Error setting parameter for field: order_id");
				return false;
			}
			try
			{
				statement.setObject(2, orderItem.getProduct_id());
			} catch (Exception e)
			{
				LOGGER.log(Level.WARNING, "Error setting parameter for field: product_id");
				return false;
			}
			
			statement.executeUpdate();
			int rowsAffected = statement.executeUpdate();
	        if (rowsAffected > 0) {
	            System.out.println("Delete successful");
	            return true; // Return true indicating success
	        } else {
	            System.out.println("No records deleted");
	            return false; // Return false indicating failure
	        }
		} catch (Exception e)
		{
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return false;
	}
	
	/**
     * Updates an existing order item in the database.
     *
     * @param orderItem the order item to update.
     * @return the updated order item.
     */
	@Override
	public OrderItem update(OrderItem orderItem)
	{
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createUpdateQuery();
		try
		{
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			try
			{
				statement.setObject(1, orderItem.getQuantity());
				
			} catch (Exception e)
			{
				LOGGER.log(Level.WARNING, "Error setting parameter for field: quantity");
			}
			try
			{
				statement.setObject(2, orderItem.getOrder_id());
			} catch (Exception e)
			{
				LOGGER.log(Level.WARNING, "Error setting parameter for field: order_id");
			}
			try
			{
				statement.setObject(2, orderItem.getProduct_id());
			} catch (Exception e)
			{
				LOGGER.log(Level.WARNING, "Error setting parameter for field: product_id");
			}
			statement.executeUpdate();
			System.out.println("Update successful");
			return orderItem;
		} catch (Exception e)
		{
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}
}
