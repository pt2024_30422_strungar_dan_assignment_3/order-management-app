package org.ordermanagement.data_access;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import org.ordermanagement.connection.ConnectionFactory;

/**
 * The AbstractDAO class provides common methods and utilities for working with database access objects.
 *
 * @param <T> the type of object managed by the DAO.
 */

public class AbstractDAO<T>
{
	protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

	protected final Class<T> type;

	/**
     * Constructs a new AbstractDAO instance.
     */
	
	@SuppressWarnings("unchecked")
	public AbstractDAO()
	{
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		// this.type = type;
	}
	
	/**
     * Creates a SELECT query to retrieve objects by a specific field value.
     *
     * @param field the field to filter the query by.
     * @return the SELECT query string.
     */

	protected String createSelectQuery(String field)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		if(type.getSimpleName().toLowerCase().equals("order")) {
			sb.append("\"");
			sb.append(type.getSimpleName().toLowerCase());
			sb.append("\"");
		}
		else {
			sb.append(type.getSimpleName());
		}
		sb.append(" WHERE " + field + " =?");
		System.out.println(sb);
		return sb.toString();
	}

	/**
     * Creates a SELECT query to retrieve all objects.
     *
     * @return the SELECT query string.
     */
	
	protected String createSelectAllQuery()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName());
		System.out.println(sb);
		return sb.toString();
	}

	/**
     * Creates an INSERT query to add a new object to the database.
     *
     * @return the INSERT query string.
     */
	
	protected String createInsertQuery()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ");
		sb.append(type.getSimpleName());
		sb.append("(");
		for (Field field : type.getDeclaredFields())
		{
			if (field.toString().toLowerCase().contains("id"))
			{
				continue;
			}
			sb.append(field.getName() + ", ");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.deleteCharAt(sb.length() - 1);
		sb.append(") VALUES (");
		for (Field field : type.getDeclaredFields())
		{
			if (field.toString().toLowerCase().contains("id"))
			{
				continue;
			}
			sb.append("?, ");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.deleteCharAt(sb.length() - 1);
		sb.append(")");
		return sb.toString();
	}

	 /**
     * Creates an UPDATE query to modify an existing object in the database.
     *
     * @return the UPDATE query string.
     */
	
	protected String createUpdateQuery()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE " + type.getSimpleName() + " ");
		sb.append("SET ");
		for(Field field : type.getDeclaredFields()) {
			if(field.getName().toLowerCase().contains("id")){
				continue;
			}
			sb.append(field.getName() + " = ?, ");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.deleteCharAt(sb.length() - 1);
		sb.append(" WHERE " + type.getDeclaredFields()[0].getName() + " = ?");
		System.out.println(sb);
		return sb.toString();
	}
	
	 /**
     * Creates a DELETE query to remove an object from the database.
     *
     * @return the DELETE query string.
     */
	
	protected String createDeleteQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE FROM ");
		if(type.getSimpleName().toLowerCase().equals("order")) {
			sb.append("\"");
			sb.append(type.getSimpleName().toLowerCase());
			sb.append("\"");
		}
		else {
			sb.append(type.getSimpleName());
		}
		sb.append(" WHERE ");
		sb.append(type.getDeclaredFields()[0].getName() + " = ?");
		System.out.println(sb.toString());
		return sb.toString();
	}
	
	 /**
     * Retrieves all objects from the database.
     *
     * @return a list of all objects.
     */
	
	public List<T> findAll()
	{
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectAllQuery();
		try
		{
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();
			return createObjects(resultSet);
		} catch (SQLException e)
		{
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll " + e.getMessage());
		} finally
		{
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	/**
     * Retrieves an object by its unique identifier.
     *
     * @param id the identifier of the object to retrieve.
     * @return the object with the specified identifier, or null if not found.
     */
	
	public List<T> findById(Integer id)
	{
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery(type.getDeclaredFields()[0].getName().toString());
		try
		{
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			return createObjects(resultSet) ;
		} catch (SQLException e)
		{
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally
		{
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	/**
     * Inserts a new object into the database.
     *
     * @param t the object to insert.
     * @return the inserted object.
     */
	
	public T insert(T t)
	{
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createInsertQuery();
		System.out.println(query);
		try
		{
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			int aux = 0;
			for (Field field : type.getDeclaredFields())
			{
				if (field.getName().toLowerCase().contains("id"))
				{
					continue;
				}
				field.setAccessible(true); // Ensure that we can access private fields if any
				try
				{
					Object value = field.get(t); // Retrieve the value of the field from the object
					statement.setObject(++aux, value);
				} catch (Exception e)
				{
					LOGGER.log(Level.WARNING, "Error setting parameter for field: " + field.getName());
				}
			}
			statement.executeUpdate();
			System.out.println("Insert successful");
			return t;
		} catch (Exception e)
		{
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally
		{
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	/**
     * Updates an existing object in the database.
     *
     * @param t the object to update.
     * @return the updated object.
     */
	
	public T update(T t)
	{
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createUpdateQuery();
		try
		{
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			int aux = 0;
			for (Field field : type.getDeclaredFields())
			{
				field.setAccessible(true); // Ensure that we can access private fields if any
				if (field.getName().toLowerCase().contains("id"))
				{
					continue;
				}
				try
				{
					Object value = field.get(t); // Retrieve the value of the field from the object
					statement.setObject(++aux, value);
				} catch (Exception e)
				{
					LOGGER.log(Level.WARNING, "Error setting parameter for field: " + field.getName());
				}
			}
			type.getDeclaredFields()[0].setAccessible(true);
			statement.setObject(++aux, type.getDeclaredFields()[0].get(t));
			type.getDeclaredFields()[0].setAccessible(false);
			statement.executeUpdate();
			System.out.println("Update successful");
			return t;
		} catch (Exception e)
		{
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	
	/**
     * Deletes an object from the database.
     *
     * @param t the object to delete.
     * @return true if the deletion was successful, false otherwise.
     */
	public boolean delete(T t) {
	    Connection connection = null;
	    PreparedStatement statement = null;
	    ResultSet resultSet = null;
	    String query = createDeleteQuery();
	    try {
	        connection = ConnectionFactory.getConnection();
	        statement = connection.prepareStatement(query);
	        Field idField = type.getDeclaredFields()[0];
	        idField.setAccessible(true);
	        try {
	            Object value = idField.get(t);
	            statement.setObject(1, value);
	        } catch (Exception e) {
	            LOGGER.log(Level.WARNING, "Error setting parameter for field: " + idField.getName(), e);
	            return false; // Return false indicating failure
	        }
	        int rowsAffected = statement.executeUpdate();
	        if (rowsAffected > 0) {
	            System.out.println("Delete successful");
	            return true; // Return true indicating success
	        } else {
	            System.out.println("No records deleted");
	            return false; // Return false indicating failure
	        }
	    } catch (SQLException e) {
	        LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage(), e);
	    } finally {
	        ConnectionFactory.close(resultSet);
	        ConnectionFactory.close(statement);
	        ConnectionFactory.close(connection);
	    }
	    return false; // Return false indicating failure
	}

	/**
     * Creates objects from a ResultSet.
     *
     * @param resultSet the ResultSet containing the data.
     * @return a list of objects created from the ResultSet.
     */
	protected List<T> createObjects(ResultSet resultSet) {
	    List<T> list = new ArrayList<>();
	    Constructor<?>[] ctors = type.getDeclaredConstructors();
	    Constructor<?> ctor = null;
	    for (Constructor<?> c : ctors) {
	        if (c.getGenericParameterTypes().length == 0) {
	            ctor = c;
	            break;
	        }
	    }

	    try {
	        while (resultSet.next()) {
	            T instance;
	            if (type.isRecord()) {
	                // Handle records
	                Constructor<?> canonicalCtor = type.getDeclaredConstructor(
	                    Stream.of(type.getRecordComponents())
	                          .map(RecordComponent::getType)
	                          .toArray(Class[]::new)
	                );
	                Object[] args = new Object[type.getRecordComponents().length];
	                for (int i = 0; i < args.length; i++) {
	                    String fieldName = type.getRecordComponents()[i].getName();
	                    Object value = resultSet.getObject(fieldName);
	                    if (type.getRecordComponents()[i].getType() == LocalDateTime.class && value instanceof Timestamp) {
	                        args[i] = ((Timestamp) value).toLocalDateTime();
	                    } else {
	                        args[i] = value;
	                    }
	                }
	                instance = (T) canonicalCtor.newInstance(args);
	            } else {
	                // Handle regular classes
	                ctor.setAccessible(true);
	                instance = (T) ctor.newInstance();
	                for (Field field : type.getDeclaredFields()) {
	                    String fieldName = field.getName();
	                    Object value = resultSet.getObject(fieldName);
	                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(fieldName, type);
	                    Method method = propertyDescriptor.getWriteMethod();
	                    if (field.getType() == LocalDateTime.class && value instanceof Timestamp) {
	                        LocalDateTime localDateTime = ((Timestamp) value).toLocalDateTime();
	                        method.invoke(instance, localDateTime);
	                    } else {
	                        method.invoke(instance, value);
	                    }
	                }
	            }
	            list.add(instance);
	        }
	    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | SQLException | IntrospectionException | NoSuchMethodException e) {
	        e.printStackTrace();
	    }
	    return list;
	}
}
