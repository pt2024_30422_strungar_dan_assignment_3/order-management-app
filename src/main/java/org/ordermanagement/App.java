package org.ordermanagement;

import javax.swing.SwingUtilities;
import org.ordermanagement.presentation.MainGUI;

/**
 * The main class for the Order Management application.
 * It initializes the GUI on the Event Dispatch Thread.
 */
public class App {

    /**
     * The main method which serves as the entry point for the application.
     * 
     * @param args Command-line arguments (not used).
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            /**
             * Runs the application by creating a new instance of MainGUI.
             */
            public void run() {
                new MainGUI();
            }
        });
    }
}
